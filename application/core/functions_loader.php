<?php
class functionsLoader {
    public static function load($dir, $ext) {
        $directory = opendir($dir);
        while($file = readdir($directory)) {
            $extension = substr($file, strlen($ext)*-1);
            if(($file != '.') && ($file != '..') && ($extension == $ext)) {
                include $dir . $file;
            }
        }
        closedir($directory);
    }
}
?>
