<?php

DB::$user = '';
DB::$password = '';
DB::$dbName = '';
DB::$host = 'localhost';
DB::$port = '3306';
DB::$encoding = 'utf8';

class Config{
  public static function get(){
    $config = array(
      'home_url' => 'https://discoverivan.ru/project/gameshop/',
      'db_prefix' => 'gshop_'
    );
    return $config;
  }
}

$cfg = new Config();

?>
