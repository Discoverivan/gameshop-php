<?php
session_start();
ini_set('display_errors', true); // выключить при публикации в сети
error_reporting(-1); // включить при публикации в сети

date_default_timezone_set('Europe/Moscow'); // установка времени

define('DOMAIN', $_SERVER['SERVER_NAME']); // определяем наш домен
define('DS', DIRECTORY_SEPARATOR); // используем разделитель системы
define('ROOT', $_SERVER['DOCUMENT_ROOT'] . DS . 'project' . DS . 'gameshop'); // корневая директория
define('DIR_APP', ROOT . DS . 'application' . DS); // пути для ядра
define('DIR_CORE', DIR_APP . 'core' . DS);
define('DIR_LIBS', DIR_APP . 'libraries' . DS);
define('DIR_ROUTES', DIR_APP . 'routes' . DS);
?>
