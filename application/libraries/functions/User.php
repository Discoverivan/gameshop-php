<?php

class User{
  public static function isLogged()
	{
    global $cfg;
    $t_prefix = $cfg->get()['db_prefix'];

    if (isset($_SESSION['id']) && isset($_SESSION['hash'])){
      $userdata = DB::query("SELECT *,INET_NTOA(ip) AS ip FROM ".$t_prefix."users WHERE id='".intval($_SESSION['id'])."' LIMIT 1");
      if (($userdata[0]['hash'] != $_SESSION['hash']) || ($userdata[0]['id'] != $_SESSION['id'])){
        unset($_SESSION['id']);
        unset($_SESSION['hash']);
        return false;
      } else {
        return true;
      }
    }
    return false;
	}

  public static function signIn(){
    $result = User::login($_POST['login'], $_POST['password']);
    header('Content-Type: application/json');
    echo json_encode($result);
  }

  public static function register(){
    $result = User::registerUser("", "");
    echo $result;
  }

  public static function logout(){
    $_SESSION = array();
    session_unset();
    session_destroy();
    Flight::redirect("/admin", 301);
    return true;
  }

  private static function login($login, $password)
	{
    global $cfg;
    $t_prefix = $cfg->get()['db_prefix'];
    $data = DB::query("SELECT id, password FROM ".$t_prefix."users WHERE name='".$login."' LIMIT 1");
	  if($data[0]['password'] === md5(md5($password)))
	  {
      $hash = md5(User::generateCode(10));

      DB::update($t_prefix.'users', array(
      'hash' => $hash
      ), "id=%s", $data[0]['id']);

      $_SESSION['id']=$data[0]['id'];
      $_SESSION['hash']=$hash;
      return true;
	  }
	  else
	  {
      return false;
	  }
	}

  private static function registerUser($login, $password)
	{
    global $cfg;
    $t_prefix = $cfg->get()['db_prefix'];

		$err = array();
    if(!preg_match("/^[a-zA-Z0-9]+$/",$login))
    {
        $err[] = "Логин может состоять только из букв английского алфавита и цифр";
    }

    if(strlen($login) < 3 or strlen($login) > 30)
    {
        $err[] = "Логин должен быть не меньше 3-х символов и не больше 30";
    }

    $query = DB::query("SELECT * FROM ".$t_prefix."users WHERE name='".$login."'");
    if(count($query) > 0)
    {
       $err[] = "Пользователь с таким логином уже существует в базе данных";
    }


    if(count($err) == 0)
    {
        $password = md5(md5(trim($password)));

        DB::insert($t_prefix.'users', array(
          'name' => $login,
          'password' => $password
        ));

        return true;
    }
    else
    {
        $s.="<b>При регистрации произошли следующие ошибки:</b><br>";
        foreach($err AS $error)
        {
            $s.="$error.<br>";
        }
    }
    return $s;
	}

  private static function generateCode($length=6) {
  	    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHI JKLMNOPRQSTUVWXYZ0123456789";
  	    $code = "";
  	    $clen = strlen($chars) - 1;
  	    while (strlen($code) < $length) {
  	            $code .= $chars[mt_rand(0,$clen)];
  	    }
  	    return $code;
  	}
}

?>
