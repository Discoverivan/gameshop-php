<?php
class twigHelper{
  public static function render($tpl, $array = null) {
        global $cfg;
        $array['assets'] =$cfg->get()['home_url'].'theme/assets/';
        $array['home_url'] = $cfg->get()['home_url'];

        $loader = new Twig_Loader_Filesystem('theme/view');
        // $twig = new Twig_Environment($loader);
        $twig = new Twig_Environment($loader, ['debug' => true]);

        $twig->addExtension(new Twig_Extension_StringLoader());
        $twig->addExtension(new Twig_Extension_Debug()); // remove this
        $template = $twig->loadTemplate($tpl);
        $template = $template->render($array);

        echo $template;
    }
}
?>
