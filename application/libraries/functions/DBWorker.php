<?php

class DBWorker{
  public static function getCatItems($option, $type=0){
    if ($type == 0){
      header('Content-Type: application/json');
      echo json_encode(DBWorker::selectCatItems($option));
    } else {
      return DBWorker::selectCatItems($option);
    }
  }

  public static function addGame(){
    echo (DBWorker::insertGame($_POST));
  }

  public static function getGame($id, $type=0){

  }

  public static function deleteGame(){
    global $cfg;
    $t_prefix = $cfg->get()['db_prefix'];

    $results = DB::delete($t_prefix.'games', "id=%s", $_POST['id']);
    echo $results;
  }

  public static function getGamesList($type=0){
    if ($type == 0){
      header('Content-Type: application/json');
      echo json_encode(DBWorker::selectGamesList());
    } else {
      return DBWorker::selectGamesList();
    }
  }

  public static function addCatItems($option){
    global $cfg;
    $t_prefix = $cfg->get()['db_prefix'];
    if (($_POST['name'] != "") || ($_POST['name'] != null)){
      $results = DB::insert($t_prefix.$option, array('name' => $_POST['name']));
      echo 1;
    } else {
      echo 0;
    }
  }

  public static function deleteCatItem($option){
    global $cfg;
    $t_prefix = $cfg->get()['db_prefix'];

    $results = DB::delete($t_prefix.$option, "id=%s", $_POST['id']);
    echo $results;
  }

  private static function selectCatItems($option){
    global $cfg;
    $t_prefix = $cfg->get()['db_prefix'];

    $results = DB::query("SELECT * FROM ".$t_prefix.$option);
    return $results;
  }

  private static function insertGame($params){
    global $cfg;
    $t_prefix = $cfg->get()['db_prefix'];

    //GAME INFO INSERT
    DB::insert($t_prefix.'games', array(
      'title' => $params['title'],
      'description' => $params['description'],
      'release_date' => $params['date'],
      'image_url' => $params['poster'],
      'price' => $params['price']
    ));
    $game_id = DB::insertId();

    //Screens insert
    $screens = array();
    if (strlen ($params['screen1']) > 0){
      $screens[] = array(
        'game_id' => $game_id,
        'image_url' => $params['screen1']
      );
    }
    if (strlen ($params['screen2']) > 0){
      $screens[] = array(
        'game_id' => $game_id,
        'image_url' => $params['screen2']
      );
    }
    if (strlen ($params['screen3']) > 0){
      $screens[] = array(
        'game_id' => $game_id,
        'image_url' => $params['screen3']
      );
    }
    if (count($screens) > 0){
      DB::insert($t_prefix.'game_screenshots', $screens);
    }

    //Insert genres, platforms, languages
    $genres = array();
    $languages = array();
    $platforms = array();
    foreach ($params['genres'] as $el) {
      $genres[] = array(
        'game_id' => $game_id,
        'genre_id' => (int)$el
      );
    }
    foreach ($params['platforms'] as $el) {
      $platforms[] = array(
        'game_id' => $game_id,
        'platform_id' => (int)$el
      );
    }
    foreach ($params['languages'] as $el) {
      $languages[] = array(
        'game_id' => $game_id,
        'language_id' => (int)$el
      );
    }
    DB::insert($t_prefix.'game_genre', $genres);
    DB::insert($t_prefix.'game_language', $languages);
    DB::insert($t_prefix.'game_platform', $platforms);
    return 1;
  }

  private static function selectGamesList(){
    global $cfg;
    $t_prefix = $cfg->get()['db_prefix'];

    $results = DB::query("SELECT * FROM ".$t_prefix.'games');

    return $results;
  }

}

?>
