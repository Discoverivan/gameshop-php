<?php
Flight::route('/', function(){
  twigHelper::render('index.tpl');
});

Flight::route('/admin', function(){
  if (User::isLogged()){
    twigHelper::render('admin/main.tpl');
  } else {
    twigHelper::render('admin/login.tpl',array());
  }
});

//ADMIN CLOSED ZONE

if (User::isLogged()){
  Flight::route('/admin/logout', function(){
    User::logout();
  });

  Flight::route('/admin/edit/@option', function($option){
    if ($option == "games"){
      $preload = array(
        'genres_list' => DBWorker::getCatItems("genres",1),
        'languages_list' => DBWorker::getCatItems("languages",1),
        'platforms_list' => DBWorker::getCatItems("platforms",1)
     );
      twigHelper::render('admin/games.tpl',$preload);
    } else
    if ($option == "genres" || $option == "languages" || $option == "platforms"){
      twigHelper::render('admin/add_category.tpl', array('type' => $option));
    } else {
      Flight::redirect('/admin');
    }
  });
}

Flight::route('/test', function(){
  $preload = array(
    'genres_list' => DBWorker::getCatItems("genres",1),
    'languages_list' => DBWorker::getCatItems("languages",1),
    'platforms_list' => DBWorker::getCatItems("platforms",1)
 );
 var_dump($preload);
});
?>
