<?php
Flight::route('/api/login', function(){
  User::signIn();
});

Flight::route('POST /api/get/cat/@option', function($option){
  DBWorker::getCatItems($option);
});

Flight::route('POST /api/add/cat/@option', function($option){
  DBWorker::addCatItems($option);
});

Flight::route('POST /api/delete/cat/@option', function($option){
  DBWorker::deleteCatItem($option);
});

Flight::route('POST /api/upload/image/', function(){
  imgurUploader::upload();
});

Flight::route('POST /api/add/game', function(){
  DBWorker::addGame();
});

Flight::route('/api/get/games', function(){
  DBWorker::getGamesList();
});

Flight::route('/api/delete/game', function(){
  DBWorker::deleteGame();
});
?>
