{% include 'header.tpl' %}

<section class="index">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <div class="left-menu">
          <div class="title">
            Поиск по жанру
          </div>
          <ul class="list">
            <li><a href="#">Гонки</a></li>
            <li><a href="#">Экшен</a></li>
            <li><a href="#">Симулятор</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-9">
        content
      </div>
    </div>
  </div>
</section>

{% include 'footer.tpl' %}
