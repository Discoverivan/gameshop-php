<!DOCTYPE html>
<html lang="ru-RU">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<title>Магазин игр</title>

<meta name="description" content="Магазин игр. Курсовой проект по базам данных." />

<meta itemprop="image" content="{{ assets }}images/icon.png" />
<link rel="icon" href="{{ assets }}images/icon.png" sizes="32x32" />
<link rel="icon" href="{{ assets }}images/icon.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="{{ assets }}images/icon.png" />
<meta name="msapplication-TileImage" content="{{ assets }}images/icon.png" />

<link rel='stylesheet' href='{{ assets }}styles/style.css' type='text/css' media='all' />
<link rel='stylesheet' href='{{ assets }}styles/bootstrap.min.css' type='text/css' media='all' />
</head>
<body>
  <section class="header">
    <div class="container">
      <div class="row">
        <div class="col-6 title">
          Магазин игр
        </div>
        <div class="col-6 buttons">
          <form class="form-inline float-right" action="" method="post">
            <input type="text" name="search" class="form-control mb-2 mr-sm-2 mb-sm-0">
            <button type="submit" class="btn btn-primary">ПОИСК</button>
          </form>
        </div>
      </div>
    </div>
    <div class="container popular">
      <div class="row">
        <div class="col-12">
          <div class="title">
            ПОПУЛЯРНЫЕ ИГРЫ
          </div>
          <div id="popular-games" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
              <div class="carousel-item active">
                <a href="#" class="game-card" style="background-image:url('http://via.placeholder.com/1280x720');">
                  <div class="game-title">
                    Название игры 1
                  </div>
                </a>
              </div>
              <div class="carousel-item">
                <a href="#" class="game-card" style="background-image:url('http://via.placeholder.com/1280x720');">
                  <div class="game-title">
                    Название игры 2
                  </div>
                </a>
              </div>
            </div>
            <a class="carousel-control-prev" href="#popular-games" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#popular-games" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>
