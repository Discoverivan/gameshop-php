<!DOCTYPE html>
<html lang="ru-RU">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<title>Магазин игр - ПУ</title>

<meta name="description" content="Магазин игр - ПУ" />

<meta itemprop="image" content="{{ assets }}images/icon.png" />
<link rel="icon" href="{{ assets }}images/icon.png" sizes="32x32" />
<link rel="icon" href="{{ assets }}images/icon.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="{{ assets }}images/icon.png" />
<meta name="msapplication-TileImage" content="{{ assets }}images/icon.png" />

<link rel='stylesheet' href='{{ assets }}styles/admin.css' type='text/css' media='all' />
<link rel='stylesheet' href='{{ assets }}styles/bootstrap.min.css' type='text/css' media='all' />
</head>
<body id="login" class="login">
<form id="login-form" class="form-signin" action="{{ home_url }}api/login" method="post">
  <img class="mb-4" src="{{ assets }}images/icon.png" alt="" width="100" height="100">
  <h1 class="h3 mb-3 font-weight-normal">Вход в ПУ</h1>

  <input type="text" id="inputEmail" name="login" class="form-control" placeholder="Имя пользователя" required autofocus>
  <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Пароль" required>
  <button class="btn btn-lg btn-primary btn-block" type="submit">Войти</button>
  <div class="alert alert-danger" role="alert" style="display:none;">
    Неправильный логин или пароль
  </div>
</form>
{% include 'admin/footer.tpl' %}
