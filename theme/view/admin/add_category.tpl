{% include 'admin/header.tpl' %}

<section id="add-category" class="add-category">
  <div class="container">
    <div class="row title">
      <div class="col-12">
        {% if type == "genres" %}
        <h1>Редактирование жанров</h1>
        {% elseif type == "platforms" %}
        <h1>Редактирование платформ</h1>
        {% elseif type == "languages" %}
        <h1>Редактирование языков</h1>
        {% endif %}
      </div>
    </div>
    <div class="row cat-list">
      <div class="col-md-8">
        {% if type == "genres" %}
        <h4>Список жанров</h4>
        {% elseif type == "platforms" %}
        <h4>Список платформ</h4>
        {% elseif type == "languages" %}
        <h4>Список языков</h4>
        {% endif %}
        <table id="{{type}}-list" class="table table-striped">
          <thead>
            <tr>
              <th>ID</th>
              <th>Название</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
          <!---  <tr>
              <td>1</td>
              <td>Test</td>
              <td class="float-right">
                <button type="button" class="btn btn-danger">Удалить</button>
              </td>
            </tr>-->
          </tbody>
        </table>
      </div>
      <div class="col-md-4">
        {% if type == "genres" %}
        <h4>Добавить жанр</h4>
        {% elseif type == "platforms" %}
        <h4>Добавить платформу</h4>
        {% elseif type == "languages" %}
        <h4>Добавить язык</h4>
        {% endif %}
        <form id="add-cat-form" action="{{ home_url }}api/add/cat/{{ type }}" method="post">
          <div class="form-group">
            <input type="text" name="name" class="form-control" placeholder="Название" required>
          </div>
          <button type="submit" class="btn btn-primary btn-block">Добавить</button>
        </form>
        <br>
        <div class="alert alert-danger" role="alert" style="display:none;">
          Такое название уже есть в базе данных
        </div>
      </div>
    </div>
  </div>
</section>

{% include 'admin/footer.tpl' %}
