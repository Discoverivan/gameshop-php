<!DOCTYPE html>
<html lang="ru-RU">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width">
<title>Маназин игр - ПУ</title>

<meta name="description" content="Маназин игр - ПУ" />

<meta itemprop="image" content="{{ assets }}images/icon.png" />
<link rel="icon" href="{{ assets }}images/icon.png" sizes="32x32" />
<link rel="icon" href="{{ assets }}images/icon.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="{{ assets }}images/icon.png" />
<meta name="msapplication-TileImage" content="{{ assets }}images/icon.png" />

<link rel='stylesheet' href='{{ assets }}styles/admin.css' type='text/css' media='all' />
<link rel='stylesheet' href='{{ assets }}styles/bootstrap.min.css' type='text/css' media='all' />
</head>
<body>
<header>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <a class="navbar-brand" href="{{ home_url }}admin">ПАНЕЛЬ УПРАВЛЕНИЯ</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Просмотр и изменение</a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="{{ home_url }}admin/edit/games">Игры</a>
            <a class="dropdown-item" href="{{ home_url }}admin/edit/genres">Жанры</a>
            <a class="dropdown-item" href="{{ home_url }}admin/edit/languages">Языки</a>
            <a class="dropdown-item" href="{{ home_url }}admin/edit/platforms">Платформы</a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{ home_url }}admin/orders">Заказы</a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" href="{{ home_url }}admin/logout">Выход</a>
        </li>
      </ul>
    </div>
  </nav>
</header>
