{% include 'admin/header.tpl' %}

<section id="add-games" class="add-games">
  <div class="container">
    <div class="row title">
      <div class="col-12">
        <h1>Редактирование списка игр</h1>
      </div>
    </div>
    <div class="row alerts">
      <div class="col-12">
        <div class="alert alert-success" role="alert" style="display:none;"></div>
        <div class="alert alert-danger" role="alert" style="display:none;"></div>
      </div>
    </div>
    <div class="row add-game" style="display:none;">
      <div class="col-12">
        <h4>Добавить игру</h4><br>
        <form  id="add-game" action="{{ home_url }}api/add/game" method="post">
          <div class="form-group">
            <label>Название</label>
            <input type="text" name="title" class="form-control" required>
          </div>
          <div class="form-group">
            <label>Дата выпуска</label>
            <input type="date" name="date" class="form-control" required>
          </div>
          <div class="form-group" id="poster">
            <label>Постер</label><br>
            <img class="img-thumbnail" src="" style="display:none;">
            <input type="file" accept="image/jpeg,image/png,image/jpg" onchange="uploadImage('poster')">
            <p class="loading" style="display:none;"><b>Загрузка картинки...</b></p>
            <div class="alert alert-danger image-upload-error" role="alert" style="display:none;"></div>
            <button class="btn btn-warning change" type="button" style="display:none;" onclick="changeImage('poster')">Изменить картинку</button>
            <input class="storage" type="hidden" name="poster">
          </div>
          <div class="form-group">
            <label>Описание</label>
            <textarea name="description" class="form-control" rows="8" required></textarea>
          </div>
          <div class="form-group">
            <label>Жанры</label>
            <select multiple class="form-control" name="genres[]" required>
              {% for genre in genres_list %}
                <option value="{{ genre.id }}">{{ genre.name }}</option>
              {% endfor %}
            </select>
          </div>
          <div class="form-group">
            <label>Языки</label>
            <select multiple class="form-control" name="languages[]" required>
              {% for language in languages_list %}
                <option value="{{ language.id }}">{{ language.name }}</option>
              {% endfor %}
            </select>
          </div>
          <div class="form-group">
            <label>Платформы</label>
            <select multiple class="form-control" name="platforms[]" required>
              {% for platform in platforms_list %}
                <option value="{{ platform.id }}">{{ platform.name }}</option>
              {% endfor %}
            </select>
          </div>
          <div class="form-group" id="screen1">
            <label>Скриншоты</label><br>
            <img class="img-thumbnail" src="" style="display:none;">
            <input type="file" accept="image/jpeg,image/png,image/jpg" onchange="uploadImage('screen1')">
            <p class="loading" style="display:none;"><b>Загрузка картинки...</b></p>
            <div class="alert alert-danger image-upload-error" role="alert" style="display:none;"></div>
            <button class="btn btn-warning change" type="button" style="display:none;" onclick="changeImage('screen1')">Изменить картинку</button>
            <input class="storage" type="hidden" name="screen1">
          </div>
          <div class="form-group" id="screen2">
            <br>
            <img class="img-thumbnail" src="" style="display:none;">
            <input type="file" accept="image/jpeg,image/png,image/jpg" onchange="uploadImage('screen2')">
            <p class="loading" style="display:none;"><b>Загрузка картинки...</b></p>
            <div class="alert alert-danger image-upload-error" role="alert" style="display:none;"></div>
            <button class="btn btn-warning change" type="button" style="display:none;" onclick="changeImage('screen2')">Изменить картинку</button>
            <input class="storage" type="hidden" name="screen2">
          </div>
          <div class="form-group" id="screen3">
            <br>
            <img class="img-thumbnail" src="" style="display:none;">
            <input type="file" accept="image/jpeg,image/png,image/jpg" onchange="uploadImage('screen3')">
            <p class="loading" style="display:none;"><b>Загрузка картинки...</b></p>
            <div class="alert alert-danger image-upload-error" role="alert" style="display:none;"></div>
            <button class="btn btn-warning change" type="button" style="display:none;" onclick="changeImage('screen3')">Изменить картинку</button>
            <input class="storage" type="hidden" name="screen3">
          </div>
          <div class="form-group">
            <label>Цена (руб.)</label>
            <input type="number" name="price" class="form-control" required>
          </div>
          <button type="submit" class="btn btn-primary">Добавить</button>
          <button class="btn btn-second" type="button" onclick="showGameAddSection()">Назад</button>
        </form>
      </div>
    </div>
    <div class="row title-second">
      <div class="col-md-8 d-none d-md-block">
        <h4>Список игр</h4>
      </div>
      <div class="col-md-4 d-none d-md-block add-btn">
        <button class="btn btn-primary" type="button" onclick="showGameAddSection()">Добавить игру</button>
      </div>
      <div class="col-12 d-block d-sm-block d-md-none d-lg-none d-xl-none">
        <h4>Список игр</h4>
        <button class="btn btn-primary btn-block" type="button" name="button" onclick="showGameAddSection()">Добавить игру</button>
      </div>
    </div>
    <div class="row games-list">
      <div class="col-12">
        <table id="{{type}}-list" class="table table-striped">
          <thead>
            <tr>
              <th>ID</th>
              <th>Название</th>
              <th>Дата выпуска</th>
              <th>Цена</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
          <!---  <tr>
              <td>1</td>
              <td>Test</td>
              <td class="float-right">
                <button type="button" class="btn btn-danger">Удалить</button>
              </td>
            </tr>-->
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>

{% include 'admin/footer.tpl' %}
