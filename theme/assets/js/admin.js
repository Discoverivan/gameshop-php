adminLoad();

function adminLoad(){
  if ($("#login").length != 0){
    var options = {
        beforeSubmit:  loginRequest,
        success:       loginResponse,
        data_type:     'json'
    };

    $('#login-form').ajaxForm(options);
  }
  if ($("#add-category").length != 0){
    var options = {
        beforeSubmit:  addCatRequest,
        success:       addCatResponse,
        data_type:     'json'
    };

    $('#add-cat-form').ajaxForm(options);
    getCatItems();
  }
  if ($("#add-games").length != 0){
    var options = {
        beforeSubmit:  addGameRequest,
        success:       addGameResponse,
        data_type:     'json'
    };

    $('#add-game').ajaxForm(options);
    getGamesList();
  }
}

/* !!! LOGIN !!! */

function loginRequest(formData, jqForm, options) {
    return true;
}

function loginResponse(responseText, statusText, xhr, $form)  {
  if (responseText == true){
    location.reload();
  } else {
    $("#login-form .alert").hide().slideDown('fast');
  }
}

/* !!! ADD CAT !!! */

function addCatRequest(formData, jqForm, options) {
    return true;
}

function addCatResponse(responseText, statusText, xhr, $form)  {
  if (responseText == 1){
    getCatItems();
  } else {
    $(".alert").slideDown('fast').delay(5000).slideUp('fast');
  }
}

function getCatItems(){
  var type = $('table').attr('id');
  type = type.substring(0,type.indexOf("-"));
  $.post(
  "../../api/get/cat/" + type,
  CatItemsResponse
  );
}

function CatItemsResponse(data)
{
  $('.cat-list table tbody').html('');
  if (data.length > 0){
    formatCatTable(data);
  } else {
    $(".cat-list table tbody").append(
    "<tr>" +
    "<td colspan='3'><strong>Список пуст.</strong></td>" +
    "</tr>");
  }
}

function formatCatTable(data){
  for (var i = 0, len = data.length; i < len; i++) {
    $(".cat-list table tbody").append(
    "<tr>" +
    "<td class='align-middle'>" + data[i]['id'] + "</td>" +
    "<td class='align-middle'>" + data[i]['name'] + "</td>" +
    "<td class='align-middle'><button type='button' class='btn btn-danger float-right' onclick='deleteCat(" + data[i]['id'] + ");'>Удалить</button>" +
    "</tr>");
  }
  $(".cat-list table tbody").append(
  "<tr>" +
  "<td colspan='6'> <strong>Всего: " + data.length + "</strong></td>" +
  "</tr>");
}

function deleteCat(cat_id){
  var type = $('table').attr('id');
  type = type.substring(0,type.indexOf("-"));
  $.post(
  "../../api/delete/cat/" + type,
  {
    id: cat_id
  },
  catDeleteResponse
  );
}

function catDeleteResponse(data){
  if (data == true){
    getCatItems();
  }
}


/* GAMES */

function getGamesList(){
  $.post(
  "../../api/get/games",
  gamesListResponse
  );
}

function gamesListResponse(data){
  $('.games-list table tbody').html('');
  if (data.length > 0){
    formatGamesTable(data);
  } else {
    $(".games-list table tbody").append(
    "<tr>" +
    "<td colspan='5'><strong>Список пуст.</strong></td>" +
    "</tr>");
  }
}

function formatGamesTable(data){
  for (var i = 0, len = data.length; i < len; i++) {
    $(".games-list table tbody").append(
    "<tr>" +
    "<td class='align-middle'>" + data[i]['id'] + "</td>" +
    "<td class='align-middle'>" + data[i]['title'] + "</td>" +
    "<td class='align-middle'>" + data[i]['release_date'] + "</td>" +
    "<td class='align-middle'>" + data[i]['price'] + "</td>" +
    "<td class='align-middle'><button type='button' class='btn btn-danger float-right' onclick='deleteGame(" + data[i]['id'] + ");'>Удалить</button>" +
    "</tr>");
  }
  $(".games-list table tbody").append(
  "<tr>" +
  "<td colspan='6'> <strong>Всего: " + data.length + "</strong></td>" +
  "</tr>");
}

function deleteGame(game_id){
  $.post(
  "../../api/delete/game",
  {
    id: game_id
  },
  gameDeleteResponse
  );
}

function gameDeleteResponse(data) {
  if (data == true){
    getGamesList();
    $(".alerts .alert-danger").html("Игра удалена успешно!");
    $(".alerts .alert-danger").slideDown("fast").delay(3000).slideUp("fast");
  }
}

var addGameSectionShow = false;
function showGameAddSection() {
  if (!addGameSectionShow){
    $(".add-game").slideDown("fast");
    $(".title-second").slideUp("fast");
    $(".games-list").slideUp("fast");
    gameCardImages = null;
    addGameSectionShow = true;
  } else {
    $(".add-game").slideUp("fast");
    $(".title-second").slideDown("fast");
    $(".games-list").slideDown("fast");
    addGameSectionShow = false;
  }
}

function uploadImage(type){
  var formData = new FormData();
  formData.append('image', $("#"+type+" input").prop('files')[0]);
  $("#"+type+" input").slideUp("fast");
  $("#"+type+" .loading").slideDown("fast");
  $("#"+type+" img").attr('src','');
  $.ajax({
    url: '../../api/upload/image/',  //Server script to process data
    type: 'POST',
    data: formData,
    contentType: false,
    processData: false,
    //Ajax events
    success: function(html){
      $("#"+type+" .loading").slideUp("fast");

      if (html!="Upload error" && html!="Image not set"){
        $("#"+type+" img").attr('src',html);
        $("#"+type+" img").slideDown("fast");
        $("#"+type+" .change").slideDown("fast");
        $("#"+type+" .storage").val(html);
      } else {
        $("#"+type+" .image-upload-error").html("Произошла ошибка при загрузке картинки.");
        $("#"+type+" .image-upload-error").slideDown("fast").delay(4000).slideUp("fast");
        $("#"+type+" input").slideDown("fast");
      }
    }
  });
}

function changeImage(type){
    $("#"+type+" input").slideDown("fast");
    $("#"+type+" img").slideUp("fast");
    $("#"+type+" .change").slideUp("fast");
    $("#"+type+" input").val('');
    $("#"+type+" .storage").val('');
}

function addGameRequest(formData, jqForm, options) {
    return true;
}

function addGameResponse(responseText, statusText, xhr, $form)  {
  if (responseText == 1){
    showGameAddSection();
    $(".alerts .alert-success").html("Игра успешно добавлена в базу данных!");
    $(".alerts .alert-success").slideDown("fast").delay(5000).slideUp("fast");
    getGamesList();
  } else {
    $(".alerts .alert-danger").html("Ошибка! Что-то пошло не так.");
    $(".alerts .alert-danger").slideDown("fast").delay(5000).slideUp("fast");
  }
}
