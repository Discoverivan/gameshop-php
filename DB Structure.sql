-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Май 10 2018 г., 10:44
-- Версия сервера: 10.1.31-MariaDB-cll-lve
-- Версия PHP: 7.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `personal-projects`
--

-- --------------------------------------------------------

--
-- Структура таблицы `gshop_games`
--

CREATE TABLE IF NOT EXISTS `gshop_games` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `release_date` date NOT NULL,
  `image_url` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gshop_game_genre`
--

CREATE TABLE IF NOT EXISTS `gshop_game_genre` (
  `game_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gshop_game_language`
--

CREATE TABLE IF NOT EXISTS `gshop_game_language` (
  `game_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gshop_game_platform`
--

CREATE TABLE IF NOT EXISTS `gshop_game_platform` (
  `game_id` int(11) NOT NULL,
  `platform_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gshop_game_screenshots`
--

CREATE TABLE IF NOT EXISTS `gshop_game_screenshots` (
  `game_id` int(11) NOT NULL,
  `image_url` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gshop_genres`
--

CREATE TABLE IF NOT EXISTS `gshop_genres` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gshop_languages`
--

CREATE TABLE IF NOT EXISTS `gshop_languages` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gshop_orders`
--

CREATE TABLE IF NOT EXISTS `gshop_orders` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gshop_order_games`
--

CREATE TABLE IF NOT EXISTS `gshop_order_games` (
  `order_id` int(11) NOT NULL,
  `game_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gshop_platforms`
--

CREATE TABLE IF NOT EXISTS `gshop_platforms` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `gshop_users`
--

CREATE TABLE IF NOT EXISTS `gshop_users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `hash` varchar(32) NOT NULL,
  `ip` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `gshop_games`
--
ALTER TABLE `gshop_games`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Индексы таблицы `gshop_game_genre`
--
ALTER TABLE `gshop_game_genre`
  ADD KEY `game_id` (`game_id`),
  ADD KEY `genre_id` (`genre_id`);

--
-- Индексы таблицы `gshop_game_language`
--
ALTER TABLE `gshop_game_language`
  ADD KEY `game_id` (`game_id`),
  ADD KEY `language_id` (`language_id`);

--
-- Индексы таблицы `gshop_game_platform`
--
ALTER TABLE `gshop_game_platform`
  ADD KEY `game_id` (`game_id`),
  ADD KEY `platform_id` (`platform_id`);

--
-- Индексы таблицы `gshop_game_screenshots`
--
ALTER TABLE `gshop_game_screenshots`
  ADD KEY `game_id` (`game_id`);

--
-- Индексы таблицы `gshop_genres`
--
ALTER TABLE `gshop_genres`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `gshop_languages`
--
ALTER TABLE `gshop_languages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `name_2` (`name`);

--
-- Индексы таблицы `gshop_orders`
--
ALTER TABLE `gshop_orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `gshop_order_games`
--
ALTER TABLE `gshop_order_games`
  ADD KEY `order_id` (`order_id`),
  ADD KEY `game_id` (`game_id`);

--
-- Индексы таблицы `gshop_platforms`
--
ALTER TABLE `gshop_platforms`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `gshop_users`
--
ALTER TABLE `gshop_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `gshop_games`
--
ALTER TABLE `gshop_games`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `gshop_genres`
--
ALTER TABLE `gshop_genres`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `gshop_languages`
--
ALTER TABLE `gshop_languages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `gshop_orders`
--
ALTER TABLE `gshop_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `gshop_platforms`
--
ALTER TABLE `gshop_platforms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `gshop_users`
--
ALTER TABLE `gshop_users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `gshop_game_genre`
--
ALTER TABLE `gshop_game_genre`
  ADD CONSTRAINT `gshop_game_genre_ibfk_1` FOREIGN KEY (`game_id`) REFERENCES `gshop_games` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `gshop_game_genre_ibfk_2` FOREIGN KEY (`genre_id`) REFERENCES `gshop_genres` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `gshop_game_language`
--
ALTER TABLE `gshop_game_language`
  ADD CONSTRAINT `gshop_game_language_ibfk_1` FOREIGN KEY (`game_id`) REFERENCES `gshop_games` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `gshop_game_language_ibfk_2` FOREIGN KEY (`language_id`) REFERENCES `gshop_languages` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `gshop_game_platform`
--
ALTER TABLE `gshop_game_platform`
  ADD CONSTRAINT `gshop_game_platform_ibfk_1` FOREIGN KEY (`platform_id`) REFERENCES `gshop_platforms` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `gshop_game_platform_ibfk_2` FOREIGN KEY (`game_id`) REFERENCES `gshop_games` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `gshop_game_screenshots`
--
ALTER TABLE `gshop_game_screenshots`
  ADD CONSTRAINT `gshop_game_screenshots_ibfk_1` FOREIGN KEY (`game_id`) REFERENCES `gshop_games` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `gshop_order_games`
--
ALTER TABLE `gshop_order_games`
  ADD CONSTRAINT `gshop_order_games_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `gshop_orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `gshop_order_games_ibfk_2` FOREIGN KEY (`game_id`) REFERENCES `gshop_games` (`id`) ON DELETE SET NULL;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
