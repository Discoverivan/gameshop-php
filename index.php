<?php
include 'application' . DIRECTORY_SEPARATOR . 'core' . DIRECTORY_SEPARATOR . 'defines.php';
require 'vendor/autoload.php';

include DIR_CORE . 'functions_loader.php';

functionsLoader::load(DIR_LIBS . DS . 'functions' . DS, 'php');

include DIR_CORE . 'configuration.php';
include DIR_ROUTES . 'web.php';
include DIR_ROUTES . 'api.php';

Flight::start();
?>
